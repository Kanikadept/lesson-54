import React from 'react';
import './Square.css';

const Square = props => {

    let squareColor = ["square"];

    if(props.squareCol === false) {
        squareColor.push("white");
    }

    return (
        <div onClick={(event) => props.squareCalChange(props.squareId, event)} className={squareColor.join(" ")}>
            {
                props.squareItem === true && props.squareCol === false ? "O" : ""
            }
        </div>
    );
};

export default Square;