import React from 'react';
import Square from "../Square/Square";
import './PlayBoard.css';

const PlayBoard = (props) => {
    return (
        <div className="play-board">
            {
                props.SquareArr.map(square => {
                    return (<Square key={square.id}
                                    squareId={square.id}
                                    squareItem={square.hasItem}
                                    squareCol={square.grey}
                                    squareCalChange={props.squareChangeCol}
                    />);
                })
            }
        </div>
    );
};

export default PlayBoard;