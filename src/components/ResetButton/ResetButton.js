import React from 'react';
import './ResetButton.css';

const ResetButton = props => {
    return (
        <button onClick={props.restartGame} className="reset-btn">Reset</button>
    );
};

export default ResetButton;