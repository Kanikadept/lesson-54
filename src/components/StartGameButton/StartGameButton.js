import React from 'react';
import './StartGameButton.css'

const StartGameButton = props => {
    const handleClick = () => {
        props.startNewGame();
    }
    return (
        <button onClick={handleClick}>Start game</button>
    );
};

export default StartGameButton;