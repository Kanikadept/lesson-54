import React from 'react';
import './Counter.css';

const Counter = props => {

    return (
        <p className="counter">Tries: {props.numCount}</p>
    );
};

export default Counter;