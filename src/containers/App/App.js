import React, {useState} from "react";
import PlayBoard from "../../components/PlayBoard/PlayBoard";
import StartGameButton from "../../components/StartGameButton/StartGameButton";
import './App.css';
import Counter from "../../components/Counter/Counter";
import ResetButton from "../../components/ResetButton/ResetButton";

const App = () => {
    const [squares, setSquares] = useState([]);
    const [counter, setCounter] = useState(0);


    const startGame = () => {
      const gameSquares = [];

      const ring = Math.floor(Math.random() * 37);

      for (let i = 0; i < 36; i++) {
          if(ring === i) {
              gameSquares.push({id: i, hasItem: true, grey: true});
          } else {
              gameSquares.push({id: i, hasItem: false, grey: true});
          }
      }

      setSquares([...squares , ...gameSquares]);
      console.log(squares);
    }
    const changeColor = (squareId, event) => {
        event.stopPropagation();
        const index = squares.findIndex(square => square.id === squareId);
        const squaresCopy = [...squares];
        squaresCopy[index].grey = false;

        setSquares(squaresCopy);
        upCounter();
    }

    const upCounter = () => {
        setCounter(counter + 1);
    }

    const resetGame = () => {

        const gameSquares = [];

        const ring = Math.floor(Math.random() * 37);

        for (let i = 0; i < 36; i++) {
            if(ring === i) {
                gameSquares.push({id: i, hasItem: true, grey: true});
            } else {
                gameSquares.push({id: i, hasItem: false, grey: true});
            }
        }
        setSquares(gameSquares);

        setCounter(0);
    }

    return <div className="App">
             <StartGameButton startNewGame={startGame}/>
             <PlayBoard SquareArr={squares} squareChangeCol={changeColor}/>
             <Counter numCount={counter}/>
             <ResetButton restartGame={resetGame}/>
           </div>
  };

export default App;
